import { Redirect, Route, Switch } from 'react-router-dom';

import MovieDetails from '../components/MovieDetails';
import MovieGrid from '../components/MovieGrid';
import NotFound from '../components/NotFound';
import Signup from '../components/Signup';

const CustomSwitch = () => (
  <>
    <Switch>
      <Route exact path='/assignment-studio-ghibli-collection/'>
        <MovieGrid />
      </Route>
      <Route path='/assignment-studio-ghibli-collection/category/:cid'>
        <MovieGrid />
      </Route>
      <Route path='/assignment-studio-ghibli-collection/movie/:mid'>
        <MovieDetails />
      </Route>
      <Route path='/assignment-studio-ghibli-collection/signup'>
        <Signup />
      </Route>
      <Route exact path='/assignment-studio-ghibli-collection/notfound'>
        <NotFound />
      </Route>
      {/* Default route */}
      <Route path='/'>
        <Redirect to='/assignment-studio-ghibli-collection/notfound' />
      </Route>
    </Switch>
  </>
);

export default CustomSwitch;
