import './App.css';

import CustomSwitch from './switch';
import navBarWithSwitch from './components/NavBar';

const App = () => (
  <>
    {/* cloudy-knoxville-gradient */}
    <div className='App mdb-color '>{navBarWithSwitch(CustomSwitch)}</div>
  </>
);

export default App;
