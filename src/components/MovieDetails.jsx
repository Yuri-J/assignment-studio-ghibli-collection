import _ from 'lodash';
import { Redirect, useParams } from 'react-router-dom';
import { React, useEffect } from 'react';
import { MDBJumbotron, MDBContainer, MDBRow, MDBCol, MDBCardTitle } from 'mdbreact';

import YouTube from 'react-youtube';

import styled from 'styled-components';

import { getMovieByID } from '../services/movies';

const MDBColS = (props) => {
  const { movie, className, children } = props;
  const MDBColStyled = styled(MDBCol)`
    background-image: url(${movie.imgDetail}), linear-gradient(to right, #2e2e2e, #ffffff);
    min-height: 86vh;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    background-blend-mode: multiply;
  `;
  return <MDBColStyled className={className}>{children}</MDBColStyled>;
};

const MovieDetails = () => {
  // Window Scroll to Top
  useEffect(() => {
    window.scroll({
      top: 0,
    });
  }, []);

  const params = useParams();
  const mid = _.get(params, 'mid');
  const movie = getMovieByID(mid);

  const opts = {
    height: '330',
    width: '460',
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 1,
    },
  };

  const onReady = (e) => {
    e.target.pauseVideo();
  };

  return (
    <>
      {_.isUndefined(movie) ? (
        <Redirect to='/notfound' />
      ) : (
        <MDBContainer>
          <MDBRow>
            <MDBCol>
              <MDBJumbotron className='p-0'>
                <MDBColS className='white-text' movie={movie}>
                  <MDBCol className='pt-2'>
                    <MDBCardTitle className='h1-responsive m-5 font-weight-bold'>{movie.title}</MDBCardTitle>
                    <MDBRow>
                      <MDBCol lg='6'>
                        <p className='mx-5 mb-5'>{movie.description}</p>
                        <MDBRow className='mx-5'>
                          <MDBCol>
                            <p className='0.6em'>
                              <span className='font-weight-bold text-nowrap'>Directed By</span>
                              <br />
                              {movie.directed}
                            </p>
                          </MDBCol>
                          <MDBCol>
                            <p className=''>
                              <span className='font-weight-bold'>Released Year</span>
                              <br />
                              {movie.released}
                            </p>
                          </MDBCol>
                          <MDBCol>
                            <p>
                              <span className='font-weight-bold'> Runtime </span>
                              <br />
                              {movie.runtime}
                            </p>
                          </MDBCol>
                        </MDBRow>
                        {/* <div className='d-flex'>
                          <p>haha</p>
                          <p>bebe</p>
                          <p>hooh</p>
                        </div> */}
                      </MDBCol>

                      <MDBCol lg='6' className='text-center'>
                        <YouTube videoId={movie.youtubePath} opts={opts} onReady={onReady} />
                      </MDBCol>
                    </MDBRow>
                  </MDBCol>
                </MDBColS>
              </MDBJumbotron>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      )}
    </>
  );
};

export default MovieDetails;
