import { MDBCardBody, MDBIcon, MDBRow, MDBCol, MDBMask, MDBView } from 'mdbreact';
import { Link } from 'react-router-dom';
import React, { useState } from 'react';

import styles from './MovieCardnGrid.styles';

import RankSVG from '../assets/rank.svg';

const MovieCard = (props) => {
  const detailsPath = `/assignment-studio-ghibli-collection/movie/${props._id}`;

  // Like function n Bookmark function

  const [liked, setLike] = useState(false);

  const toggleLike = () => {
    setLike((prev) => !prev);
    console.log('Like is clicked.');
  };

  const [bookmark, setBookmark] = useState(false);

  const toggleBookmark = () => {
    setBookmark((prev) => !prev);
    console.log('Bookmark is clicked.');
  };

  return (
    <>
      <styles.DivS>
        <styles.MDBCardS className='mx-auto'>
          <Link to={detailsPath}>
            <MDBView hover>
              <styles.MDBCardImageS className='img-fluid' src={props.img} waves />

              <MDBMask
                className='flex-center pl-2'
                overlay='stylish-strong'
                style={{ cursor: 'pointer', borderRadius: '1.8rem' }}
              >
                <p className='white-text'>{props.summary}</p>
              </MDBMask>
            </MDBView>
          </Link>
          <MDBCardBody className='py-2 blue-grey lighten-4'>
            <MDBRow>
              <MDBRow>
                <MDBCol className='pt-1'>
                  <img src={RankSVG} alt='' style={{ width: '3rem' }} />
                  {/* <MDBIcon icon='flag' size='lg' className='pl-3 yellow-text' /> */}
                </MDBCol>
                <MDBCol className='p-0'>
                  <h5 className='pt-1'>{props.rank}</h5>
                </MDBCol>
              </MDBRow>
              <MDBCol></MDBCol>

              <MDBCol>
                <styles.MDBIconS
                  onClick={toggleLike}
                  icon='heart'
                  size='2x'
                  className={!liked ? 'grey-text' : 'deep-orange-text'}
                  style={{ color: '#585858' }}
                ></styles.MDBIconS>
              </MDBCol>
              <MDBCol>
                <styles.MDBIconS
                  onClick={toggleBookmark}
                  icon='bookmark'
                  size='2x'
                  className={bookmark ? 'cyan-text' : 'grey-text'}
                ></styles.MDBIconS>
              </MDBCol>
            </MDBRow>

            {/* <styles.MDBCardTitleS className='py-1 text-center'>{props.title}</styles.MDBCardTitleS> */}
          </MDBCardBody>
        </styles.MDBCardS>
      </styles.DivS>
    </>
  );
};

export default MovieCard;
