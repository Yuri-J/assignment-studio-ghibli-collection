import _ from "lodash";
import { useHistory } from "react-router-dom";
import React from "react";

import { MDBDropdownItem, MDBDropdownMenu } from "mdbreact";

import { getCategories } from "../services/categories";

const CategoryDropdown = () => {
  const history = useHistory();

  return (
    <MDBDropdownMenu>
      {_.map(getCategories(), (c) => (
        <MDBDropdownItem
          key={c._id}
          onClick={() =>
            history.push(
              `/assignment-studio-ghibli-collection/category/${c._id}?page=1`
            )
          }
          className="text-center"
        >
          {c.name}
        </MDBDropdownItem>
      ))}
    </MDBDropdownMenu>
  );
};

export default CategoryDropdown;
